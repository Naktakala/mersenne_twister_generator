#ifndef _MT19937_RN_GENERATOR
#define _MT19937_RN_GENERATOR

#include<random>

/**General object that applies the Mersenne Twister algorithm.
 *
The special properties of the Mersenne Twister algorithm is that the numbers
have a massive period (i.e. the time it takes to repeat the same sequence.

\author Jan*/
class MT19937RandomNumberGenerator {
private:
    std::mt19937                            engine;         ///< The core engine
    std::uniform_real_distribution<double>  distribution;   ///< Random number distribution (Can be changed)
public:
            MT19937RandomNumberGenerator();
    double  Rand();
    void    Seed(int value);
};

/** Constructor applying a default seed of 0 and a real interval 0.0 to 1.0
\author Jan*/
MT19937RandomNumberGenerator::MT19937RandomNumberGenerator() {
    distribution = std::uniform_real_distribution<double>(0.0,1.0);
    engine.seed(0);
}

/**Returns a random number sampled from a uniform distribution.

\return Random number
\author Jan*/
double MT19937RandomNumberGenerator::Rand() {
    return distribution(engine);
}


/**Seeds the generator with a different seed.
 *
\param value int An integer seed value.

\author Jan*/
void MT19937RandomNumberGenerator::Seed(int value) {
    engine.seed(value);
}

#endif
