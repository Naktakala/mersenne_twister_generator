#include<stdio.h>
#include"mt19937_rn_generator.h"

int main()
{
    MT19937RandomNumberGenerator rn_gen1;
    MT19937RandomNumberGenerator rn_gen2;

    for (int k=0;k<10;k++) {
        printf("Generator 1 = %.10f Generator 2 = %.10f\n",rn_gen1.Rand(),rn_gen2.Rand());
    }

    printf("\n Reseeding generator 1 to original and generator 2 to something else\n\n");

    rn_gen1.Seed(0);
    rn_gen2.Seed(1);

    for (int k=0;k<10;k++) {
        printf("Generator 1 = %.10f Generator 2 = %.10f\n",rn_gen1.Rand(),rn_gen2.Rand());
    }

    return 0;
}
